#include <arduinoFFT.h>

#define SAMPLES 64
#define columns 8
#define rows 8
#define PRECALIBRATE_MODE 1
#define DYNAMIC_MODE 2
#define PEAK_MODE 3

double vReal[SAMPLES];
double vImag[SAMPLES];
char data_avgs[columns];
int max_rate[columns] = {31, 6, 4, 5, 2, 1, 2, 2};
int prev_rate[columns];
int min_rate[columns] = {0, 0, 0, 0, 0, 0, 0, 0};
unsigned long seconds;
int values[columns];
int MODE = PRECALIBRATE_MODE;

arduinoFFT FFT = arduinoFFT();

// 8x8 LED config pins
// 2-dimensional array of row pin numbers:
const int row[8] = {
  2, 7, 19, 5, 13, 18, 12, 16
};

// 2-dimensional array of column pin numbers:
const int col[8] = {
  6, 11, 10, 3, 17, 4, 8, 9
};

// 2-dimensional array of pixels:
int pixels[8][8];

void setup_led() {
    // initialize the I/O pins as outputs iterate over the pins:
    for (int thisPin = 0; thisPin < 8; thisPin++) {
        // initialize the output pins:
        pinMode(col[thisPin], OUTPUT);
        pinMode(row[thisPin], OUTPUT);
        // take the col pins (i.e. the cathodes) high to ensure that the LEDS are off:
        digitalWrite(col[thisPin], HIGH);
    }

    // initialize the pixel matrix:
    for (int x = 0; x < 8; x++) {
        for (int y = 0; y < 8; y++) {
            pixels[x][y] = HIGH;
        }
    }
}

void setup() {
    ADCSRA = 0b11100101;
    ADMUX = 0b00000000;
    delay(50);
    setup_led();
    Serial.begin(9600);
}

void loop() {
    // ++ Sampling
    for(int i=0; i<SAMPLES; i++)
    {
        while(!(ADCSRA & 0x10));
        ADCSRA = 0b11110101;
        int value = ADC - 512;
        vReal[i]= value/8;
        vImag[i] = 0;         
    }

    // ++ FFT
    FFT.Windowing(vReal, SAMPLES, FFT_WIN_TYP_HAMMING, FFT_FORWARD);
    FFT.Compute(vReal, vImag, SAMPLES, FFT_FORWARD);
    FFT.ComplexToMagnitude(vReal, vImag, SAMPLES);
    // -- FFT

    // ++ re-arrange FFT result to match with no. of columns on display
    int step = (SAMPLES/2)/rows; 
    int c=0;
    for(int i=0; i<(SAMPLES/2); i+=step)  
    {
        data_avgs[c] = 0;
        for (int k=0 ; k< step ; k++) {
            data_avgs[c] = data_avgs[c] + vReal[i+k];
        }
        data_avgs[c] = data_avgs[c]/step;
        c++;
    }

    // ++ send to display according measured value 
    for(int i=0; i<columns; i++)
    {
        data_avgs[i] = constrain(data_avgs[i], 0, 80);
        int data = data_avgs[i];
        int max_rating = max_rate[i];
        if (MODE == PEAK_MODE) {
            max_rating = 6;
        }
        values[i] = map(data, min_rate[i], max_rating, 0, columns);
//        Serial.print(String(values[i]) + ", ");
    }
//    Serial.println("");
    if (MODE == DYNAMIC_MODE) {
        int highest = values[0];
        for (int x=1; x<columns; x++) {
            highest = max(highest, values[x]);
        }
        for (int x=0; x<columns; x++) {
            values[x] = map(values[x], 0, highest, 0, columns);
            Serial.print(String(values[x]) + ", ");
        }
        Serial.println("");
    }

    if (values[0] == 0) {
        for (int x=1; x<columns-1; x++) {
            values[x-1] = values[x];
            Serial.print(String(values[x-1]) + ", ");
        }
        Serial.println("");
    }

    plot_led_values();
    refresh_screen();
}

void plot_led_values() {
    for (int x=0; x<columns; x++) {
        int val = values[x];
        for (int c=0; c<columns; c++) {
            pixels[x][c] = HIGH;
        }
        for (int c=0; c<val; c++) {
            pixels[x][c] = LOW;
        }
    }
}

void refresh_screen() {
    // iterate over the rows (anodes):
    for (int thisRow = 0; thisRow < 8; thisRow++) {
        // take the row pin (anode) high:
        digitalWrite(row[thisRow], HIGH);
        // iterate over the cols (cathodes):
        for (int thisCol = 0; thisCol < 8; thisCol++) {
            // get the state of the current pixel;
            int thisPixel = pixels[thisRow][thisCol];
            // when the row is HIGH and the col is LOW,
            // the LED where they meet turns on:
            digitalWrite(col[thisCol], thisPixel);
            // turn the pixel off:
            if (thisPixel == LOW) {
                digitalWrite(col[thisCol], HIGH);
            }
        }
        // take the row pin low to turn off the whole row:
        digitalWrite(row[thisRow], LOW);
    }
}
